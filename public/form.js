/* global document localStorage window */
function pageLoad() {
  const titleInput = document.getElementById('titleInput');
  const bodyInput = document.getElementById('noteInput');
  const btn = document.getElementById('submit');
  const form = document.querySelector('.form');
  const currentUl = document.getElementById('currentUl');
  let notes = [];
  const liIdPrefix = 'note_';
  const notesKey = 'notes';
  const maxIdKey = 'maxId';

  function dataUpdate() {
    localStorage.setItem(notesKey, JSON.stringify(notes));
  }

  function setNote(id, title, body) {
    return `
        <li id='${liIdPrefix}${id}' class='flex items-center bb b--black-10 ph0-l lh-copy bg-washed-blue'>
          <div class='pl3 flex-auto'>
            <h1 class='db black-70'>${title}</h1>
            <p class='db black-70'>${body}</p>
          </div>

          <div class='mright'>
            <a href="#" class="dlt">
              <i class="mright fa fa-trash-alt moon-gray hover-light-red pointer v-mid">
              </i>
            </a>
          </div>
        </li>
      `;
  }

  function removeNote(event) {
    event.preventDefault();

    const aElement = event.target.parentElement;
    if (!aElement.classList.contains('dlt')) return;

    const liElement = aElement.parentElement.parentElement;

    const liElementId = liElement.id;

    const noteIndex = notes.findIndex((note) => {
      return note.index === Number(liElementId.substring(liIdPrefix.length));
    });

    notes.splice(noteIndex, 1);

    liElement.remove();
    dataUpdate();
  }

  function addNote() {
    if (!bodyInput.value) {
      alert('Write Something!');
      return;
    }

    let maxId = Number(localStorage.getItem(maxIdKey));
    notes.push({ index: maxId, title: titleInput.value, body: bodyInput.value });
    currentUl.innerHTML += setNote(maxId, titleInput.value, bodyInput.value);
    maxId += 1;
    localStorage.setItem(maxIdKey, maxId);
    
    dataUpdate();
    
    form.reset();
  }

  function displayNotes() {
    notes = JSON.parse(localStorage.getItem(notesKey));
    for (let i = 0; i < notes.length; i += 1) {
      currentUl.innerHTML += setNote(notes[i].index, notes[i].title, notes[i].body);
    }
  }
  currentUl.addEventListener('click', removeNote);

  btn.addEventListener('click', addNote);
  if (localStorage.length > 0) {
    displayNotes();
  }
  form.addEventListener('submit', (e) => { e.preventDefault(); });
}
window.addEventListener('load', pageLoad);
